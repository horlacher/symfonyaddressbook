# Symfony Address Book (sab)

This is a exercise of building a symfony app for an address book management.

Technology:
- Symfony 5
- docker images: nginx, PHP 7.4, MySQL 8, node, mailhog
- Composer 
- Frontend: yarn, Webpack [Encore](https://symfony.com/doc/current/frontend.html), CSS framework [Bulma](https://bulma.io/)

## dev install

Local installation of docker is required. Then start docker-compose, followed by loading the composer modules:
```shell
docker-compose up -d
docker exec -it sab-php74 composer install
```
As soon as the composer modules are installed, init the database and apply the migrations:
```shell
docker exec -it sab-php74 php bin/console doctrine:database:create
docker exec -it sab-php74 php bin/console doctrine:migrations:migrate
```

Generate UI assets:
```shell
docker exec -it sab-node yarn install
docker exec -it sab-node yarn encore dev
```
The application is now accessible on the current machine [http://localhost:8080/](http://localhost:8080)

## Possible improvements

There are no Twig templates for Bulma available, the form layout should be better adjusted. Get icon font FontAwesome to work with Webpack Encore. Tried to embed [BazingaFakerBundle](https://github.com/willdurand/BazingaFakerBundle) to generate demo entries, but it's not compatible with the current version of Symfony, it should work with fixtures instead.