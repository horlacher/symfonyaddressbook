/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// fortawesome
import { library, dom } from "@fortawesome/fontawesome-svg-core";
import { faArrowAltCircleLeft } from "@fortawesome/free-regular-svg-icons/faArrowAltCircleLeft";
import { faTimesCircle } from "@fortawesome/free-regular-svg-icons/faTimesCircle";
import { faCheckCircle } from "@fortawesome/free-regular-svg-icons/faCheckCircle";
import { faImage } from "@fortawesome/free-regular-svg-icons/faImage";
import { faPlusSquare } from "@fortawesome/free-regular-svg-icons/faPlusSquare";
import { faPaperPlane } from "@fortawesome/free-regular-svg-icons/faPaperPlane";

library.add(faArrowAltCircleLeft, faTimesCircle, faCheckCircle, faImage, faPlusSquare, faPaperPlane );
dom.watch();

// start the Stimulus application
import './bootstrap';
